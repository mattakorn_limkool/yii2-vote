<?php

namespace obbz\vote\assets;

use yii\web\AssetBundle;

/**

 * @package obbz\vote\assets
 */
class VoteAsset extends AssetBundle
{
    public $sourcePath = '@obbz/vote/assets/static';
    public $css = [
        'vote.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
