<?php

namespace obbz\vote\controllers;

use obbz\vote\actions\VoteAction;
use Yii;
use yii\web\Controller;

/**

 * @package obbz\vote\controllers
 */
class DefaultController extends Controller
{
    /**
     * @var string
     */
    public $defaultAction = 'vote';

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'vote' => [
                'class' => VoteAction::class,
            ]
        ];
    }
}
