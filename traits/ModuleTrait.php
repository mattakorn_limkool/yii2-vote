<?php

namespace obbz\vote\traits;

use Yii;
use obbz\vote\Module;
use yii\base\InvalidConfigException;

/**
 * Trait ModuleTrait
 * @package obbz\vote\traits
 */
trait ModuleTrait
{
    /**
     * @return \obbz\vote\Module|\yii\base\Module
     * @throws InvalidConfigException
     */
    public function getModule()
    {
        if (Yii::$app->hasModule('vote') && ($module = Yii::$app->getModule('vote')) instanceof Module) {
            return $module;
        }

        throw new InvalidConfigException('Module "vote" is not set.');
    }
}
