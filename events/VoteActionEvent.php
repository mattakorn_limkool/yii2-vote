<?php

namespace obbz\vote\events;

use obbz\vote\models\VoteForm;
use yii\base\Event;

/**
 * Class VoteActionEvent
 * @package obbz\vote\events
 */
class VoteActionEvent extends Event
{
    /**
     * @var VoteForm
     */
    public $voteForm;

    /**
     * @var array
     */
    public $responseData;
}
